﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(GameManagerScript))]
public class BuildingPhaseScript : MonoBehaviour {

    public GameObject DayTimeUiCanvas;

    GameManagerScript GameLoopManager;
    GameUIScript UIScript;

    public float HowManyHoursInADay;
    public float TimeLeftInTheDay;

    bool InBuildingPhase = false;
    bool BuildingSelected;

	// Use this for initialization
	void Start () {
        GameLoopManager = GetComponent<GameManagerScript>();
        UIScript = FindObjectOfType<GameUIScript>();
	}
	
	// Update is called once per frame
	void Update () {
        if (InBuildingPhase)
        {
            if (TimeLeftInTheDay > 0.0f)
            {
                TimeLeftInTheDay -= Time.deltaTime;
            }
            else
            {
                EndDay();
            }

            checkedMouse();
        }
	}

    void checkedMouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray mRay= Camera.main.ScreenPointToRay(Input.mousePosition);
            // RaycastHit hit = new RaycastHit();
            RaycastHit hit;
            Debug.DrawRay(mRay.origin, mRay.direction, Color.red, 50.0f);
            if (Physics.Raycast(mRay.origin, mRay.direction, out hit, 1000.0f))
            {
                if (hit.transform.parent != null)
                {
                    if (hit.transform.parent.name == "House")
                    {
                        UIScript.PopUpScrapOption(hit.transform.parent.gameObject, new Vector2(Input.mousePosition.x, Input.mousePosition.y));
                        BuildingSelected = true;
                    }
                    else if (BuildingSelected == true)
                    {
                        UIScript.DeSelectedScrapOption();
                        BuildingSelected = false;
                    }
                }
            }
        }
    }

    public void NewDay()
    {
        DayTimeUiCanvas.SetActive(true);
        InBuildingPhase = true;
        TimeLeftInTheDay = HowManyHoursInADay;
    }

    public void EndDay()
    {
        DayTimeUiCanvas.SetActive(false);
        InBuildingPhase = false;
        GameLoopManager.EndBuildingPhase();
    }
}
