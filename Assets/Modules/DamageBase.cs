﻿using UnityEngine;
using System.Collections;

// Zombie grouping causes potentially:
// DIFFICULTY MODIFIER:
// health increase,
// speed increase,
// increased attack damage per hit 

/// <summary>
/// The damage base for the game.
/// </summary>
public class DamageBase : MonoBehaviour
{
    /// <summary>
    /// The health of the object this script is attached to.
    /// </summary>
    public float mHP = 100.0f;
    /// <summary>
    /// Is the object alive?
    /// </summary>
    public bool mAlive = true;

    /// <summary>
    /// Returns the over damage amount which can be used for physical forces which can be exagerated on the char if they recieve over damage.
    /// Over damage is when the hp hits 0 it's the amount of damage left to inflict.
    /// Takes parameter of the original damager object.
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="gameobject"></param>
    /// <returns></returns>    
    public float Damage(float amountofdamage, GameObject damager )
    {
        // Check if the object is dead.
        if (!mAlive) { Debug.Log("Object is already dead!");  return 0; }

        // Make sure this isn't null.
        if (damager == null)
        {
            Debug.Log("Object damaged by: " + damager.name);
        }
        // TBI: damager.

        // Calculate the new hp of the object.
        float newHP = mHP - amountofdamage;

        // if the amount of damage exceeedes the object's health then return the over damage amount which can then trigger other game events.
        if(amountofdamage > mHP)
        {
            // Death event, trigger for scoring system.
            Death();           

            // This will be negative since overdamage has been done.
            return -newHP;
        }
        else
        {
            // Negate the damage.
            mHP -= amountofdamage;
            // return 0 no overdamage, since the object wasn't killed.
            return 0;
        }
    }

    /// <summary>
    /// Call this to heal the object with x amount of hp.
    /// </summary>
    /// <param name="amount"></param>
    void Heal(float amount)
    {
        mHP = Mathf.Clamp(amount, 0.0f, 100.0f);
        Debug.Log("Healing object: " + gameObject.name);
        // On heal callback handler
        if (OnHeal != null)
        {
            // Call assigned callback.
            OnHeal();
        }
    }

    /// <summary>
    /// Called when the object dies.
    /// </summary>
    void Death()
    {
        // Increment score counter.
        Debug.Log("Death event triggered: " + gameObject.name);

        // Increment kills
        ++ScoreBase.PlayerScore.mPlayerKills;

        // Add points to the player.
        ScoreBase.PlayerScore.AddPoints(ScoreBase.PlayerScore.KILLSCORE, "<color=green>Eureka!</color> another one bites the dust, Good job!");

        // actually kill the object.
        mAlive = false;

        // Call explode effect, / whatever.
        if (OnDeath != null)
        {
            // Call assigned callback.
            OnDeath();
        }
    }

    /// <summary>
    /// Standard delegate / callback event which can be used with /event for quick implmentation
    /// </summary>
    public delegate void StandardCallback();
    
    /// <summary>
    /// 
    /// </summary>
    public event StandardCallback OnDeath;
    public event StandardCallback OnHeal;

}
