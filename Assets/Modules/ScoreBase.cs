﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The scoring system for the game, avoiding the horrible use of static variables because we didn't want to use a reference on every single game object which accumulates points.
/// The lookup time would be extremely slow if you had 20 objects search for the same reference, this can be handled by the C# JIT Compiler.
/// </summary>
public class ScoreBase : MonoBehaviour {

    void Awake()
    {
        PlayerScore = this;
       
    }
    // This is the required order, please don't change this.
    void Start()
    {
        UpdateScoreInfo(); // DEFAULT VALUE SEND.
    }
    /// <summary>
    /// Our player's score
    /// </summary>
    public static ScoreBase PlayerScore;


    public GameObject UIEndCondition;


    /// <summary>
    /// The XP the player has.
    /// </summary>
    public float mXP = 0.0f;

    /// <summary>
    /// The kills the player has made.
    /// </summary>
    public int mPlayerKills = 0;

    /// <summary>
    /// Civilians saved
    /// </summary>
    public int mPlayerCivsSaved = 0;

    /// <summary>
    /// The constant value used for the scrap value.
    /// </summary>
    public float SCRAPSCORE = 1.0f;

    /// <summary>
    /// The scrap you retreive for killing an NPC
    /// </summary>
    public float SCRAPforKILL = 7.5f;
    /// <summary>
    /// The save score from saving a civilian
    /// </summary>
    public float SAVECIVILIANSCORE = 1.0f;

    /// <summary>
    /// Placement bonus when you place a building 
    /// </summary>
    public float PLACEMENTBONUS = 5.0f;

    /// <summary>
    /// The points of XP allocated for a kill, multiple kills will give you more.
    /// </summary>
    public float KILLSCORE = 5.0f;

    /// <summary>
    /// IMPLEMNT ONLY IF THEIR IS TIME
    /// for every 5 points you will recieve a bonus of a set amount.
    /// </summary>
    public float KILLMULTIPLIER = 5.0f;

    /// <summary>
    /// The scrap quantity
    /// </summary>
    public float scrapQuantity = 10.0f;

    /// <summary>
    /// Default scrap amount.
    /// </summary>
    /// <param name="amount"></param>
    public void StatingLevelAmount()
    {
        // Update the scrap metal and xp values.
        UpdateScoreInfo();
    } 
    
    /// <summary>
    /// Add scrap
    /// </summary>
    /// <param name="amount"></param>
    public void AddScrap(int amount)
    {
        // Increment the scrap quantity
        scrapQuantity += amount;
        AddPoints(SCRAPSCORE, "We've obtained more scrap metal! +<color=red>" + amount + " SC</color>");
    }

    /// <summary>
    /// Consume some scrap boy
    /// You also gain XP for this.
    /// </summary>
    /// <param name="cost"></param>
    public bool ConsumeScrap(int cost)
    {
        if (scrapQuantity >= cost)
        {
            scrapQuantity -= cost;

            // You get points for scrap consumed!
            AddPoints(SCRAPSCORE, "Commander that's some of our scrap used, please be careful how much you use! <color=red>" + cost + " SC</color>");
            UpdateScoreInfo();
            return true;
        }
        else
        {
            GameInfoDisplay.display.AddMessage("You can't afford that right now; you need: " + cost + " SC.");
        }

        return false;
        
    }

    /// <summary>
    /// Add points to the game
    /// </summary>
    /// <param name="points"></param>
    public void AddPoints(float points, string INFO)
    {
        GameInfoDisplay.display.AddMessage(INFO +" <color=red>" + points +" XP; plus 1 SC!</color>" );
        mXP += points;

        // DO NOT USE ADD SCRAP THIS WILL INFINITE LOOP.
        // SCRAP 4 KILLZ 
        scrapQuantity += SCRAPforKILL;

        Debug.Log("INFO: " + INFO);
        Debug.Log(points);
        // Scoring sound.
        // info prompt ui
        UpdateScoreInfo();

    }

    /// <summary>
    /// Designed to avoid stupid ui draw call slowdowns.
    /// </summary>
    public void UpdateScoreInfo()
    {
        if (OnScoreUpdate != null)
        {
            OnScoreUpdate(mXP, scrapQuantity);
        }
    }

    public int EnemyPassAmount = 3;
    int OnesThatsPassAmount = 3;

    /// <summary>
    /// Designed to avoid stupid ui draw call slowdowns.
    /// </summary>
    public void EnemyMadeItToTheEnd()
    {
        OnesThatsPassAmount -= 1;
        if  (OnesThatsPassAmount <= 0) {
            OnesThatsPassAmount = EnemyPassAmount;
            CivsChangeAmount(-1);
        }
    }

    /// <summary>
    /// Designed to avoid stupid ui draw call slowdowns.
    /// </summary>
    public void CivsChangeAmount(int AmountOfChange)
    {
        PlayerScore.mPlayerCivsSaved += AmountOfChange;

        if (PlayerScore.mPlayerCivsSaved <= 0)
        {
            UIEndCondition.SetActive(true);
            PlayerScore.mPlayerCivsSaved = 0;
            GetComponent<GameManagerScript>().EndGame();
        }
    }

    //XP EVENT CALLED WHEN THE SCORE UPDATES OR SCRAP
    public delegate void GenericXPEvent(float xp, float sc);
    public event GenericXPEvent OnScoreUpdate;
}
