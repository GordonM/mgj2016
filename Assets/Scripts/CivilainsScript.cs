﻿using UnityEngine;
using System.Collections;

public class CivilainsScript : MonoBehaviour {

    public float MaxSpeed;
    public float Acceleration;
    float CurrentSpeed;
    float randSpeed;
    
    float ROTATION_SPEED = 90;
    float RADIUS = 0.5f;

    Quaternion ROTATE_LEFT = Quaternion.Euler(0, -90, 0);
    Quaternion ROTATE_RIGHT = Quaternion.Euler(0, 90, 0);

    bool RunningFromDanager;    

	// Use this for initialization
	void Start () {
        
        randSpeed = Random.Range(0, 5);
        RunningFromDanager = true;
    }
	
	// Update is called once per frame
	void Update () {
        if(RunningFromDanager)
        {
            RunToSafty();
        }	
	}

    void RunToSafty()
    {
        if (CurrentSpeed > MaxSpeed + randSpeed)
        {
            CurrentSpeed = MaxSpeed + randSpeed;
        }
        else
        {
            CurrentSpeed += Acceleration * Time.deltaTime;
        }

        float totalDist = CurrentSpeed * Time.deltaTime;

        
        /*if (transform.position.x < MinBoundingBoxPos.x || transform.position.x > MaxBoundingBoxPos.x)
        {
            ScoreBase.PlayerScore.AddPoints(ScoreBase.PlayerScore.SAVECIVILIANSCORE, "You saved a civilian!");
            Destroy(this.gameObject);
        }

        if (transform.position.z < MinBoundingBoxPos.y || transform.position.z > MaxBoundingBoxPos.y)
        {
            Debug.Log("Destroy ME Z");
        }*/
        

        while (totalDist > 0)
        {
            RaycastHit hit;
            if (!Physics.SphereCast(transform.position, RADIUS, transform.forward, out hit, totalDist))
            {


                transform.position = transform.position + (transform.forward * totalDist);

                if (!Physics.SphereCast(transform.position, RADIUS - 0.1f, Vector3.right, out hit, 1))
                {
                    Vector3 right = new Vector3(1, 0, 0).normalized;
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(right), ROTATION_SPEED * Time.deltaTime);
                }

                return;
            }

            transform.position = transform.position + (transform.forward * hit.distance);
            totalDist -= hit.distance;



            float angle = Vector3.Angle(transform.forward, hit.normal);
            angle *= Mathf.Sign(Vector3.Cross(transform.forward, hit.normal).y);

            if (angle < 0)
            {
                transform.rotation = Quaternion.LookRotation(ROTATE_RIGHT * hit.normal, Vector3.up);
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(ROTATE_LEFT * hit.normal, Vector3.up);
            }
        }
    }

    public void DayHasEnded()
    {
        RunningFromDanager = true;
    }
}
