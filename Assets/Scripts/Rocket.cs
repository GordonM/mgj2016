﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

    float speed = 25f;
    float lifespan = 10f;
    float explosionRadius = 10f;
    float detonationRadius = 2f;
    public LayerMask mask;
    float damage = 50f;
    public AudioSource audioExplode;

	// Use this for initialization
	void Start () 
    {
        StartCoroutine(Despawn());
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position += transform.forward * speed * Time.deltaTime;
        Collider[] hit = Physics.OverlapSphere(transform.position, detonationRadius, mask.value);
        if (hit.Length == 0) return;

        PlayClipAtPoint(audioExplode.clip, transform.position);
        DamageArea();
        Destroy(gameObject);
        
	}

    void DamageArea()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, explosionRadius, mask.value);
        foreach(Collider hit in hitColliders)
        {
            DamageBase script = hit.GetComponent<DamageBase>();
            if (!script) return;
            script.Damage(damage, gameObject);
        }
    }

    IEnumerator Despawn()
    {
        yield return new WaitForSeconds(lifespan);
        PlayClipAtPoint(audioExplode.clip, transform.position);
        DamageArea();
        Destroy(gameObject);
    }

    void PlayClipAtPoint(AudioClip clip, Vector3 pos)
    {
        GameObject temp = new GameObject("TempAudio");
        temp.transform.position = pos;
        AudioSource audioSource = temp.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.outputAudioMixerGroup = audioExplode.outputAudioMixerGroup;
        audioSource.Play();
        Destroy(temp, clip.length);
    }
}
