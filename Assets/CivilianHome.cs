﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(DamageBase))]
public class CivilianHome : MonoBehaviour {

    /// <summary>
    /// Civilians in the home
    /// </summary>
    int CiviliansHome = 4;

    void ZombieDamage( GameObject zObj )
    {
        GameInfoDisplay.display.AddMessage("Warning: " + gameObject.name + " taking damage... send help!");
        damageBase.Damage(40.0f, zObj );
    
    }

    void OnDeath()
    {
        GameInfoDisplay.display.AddMessage("You just lost " + CiviliansHome + " civilians, be more careful commander.");
        // Kill civilians, remove them from our population.
        ScoreBase.PlayerScore.CivsChangeAmount(-CiviliansHome);

        Destroy(this.gameObject);
        
    }

    DamageBase damageBase;
	// Use this for initialization
	void Start () {
        damageBase = GetComponent<DamageBase>();
        damageBase.OnDeath += OnDeath;

        CiviliansHome = Random.Range(1, 5);
        // Since the player is only shown the civilians saved at the end no point in not doing it this way, one variable saves the day.

        ScoreBase.PlayerScore.CivsChangeAmount(CiviliansHome);

    }

    // Damage range, e.g. damaged when the enemy gets within this range.
    public float DamageRange = 4.5f;
	// Update is called once per frame
	void FixedUpdate () {

        Enemy[] enemies = FindObjectsOfType<Enemy>();

        // Check for enemy outside our door.
        foreach( Enemy ent in enemies )
        {
            float distance = Vector3.Distance(transform.position, ent.gameObject.transform.position);
            if (distance <= DamageRange)
            {
                ZombieDamage(ent.gameObject);
            }
        }

    }
}
