﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OutputScore : MonoBehaviour {
    private Text mOutput;
	// Use this for initialization
	void Start()
    {
        // Assign reference for later use.
        mOutput = GetComponent<Text>();
       
        // Clamp between number for grades, abcde
        // e.g. if you fail to stop more than 10 zombies your grade will be pushed down to B.
        // then further on the psycologyical mechanic will fuck with the player, as they scored high amounts of XP, but WHY didn't they succeede.
        // They simply aren't good enough at the game.

	}
	
	// Update is called once per frame
	void FixedUpdate () {
        mOutput.text = "<b>Game Stats:</b>\n\n"
           + "<color=red><i>Zombies killed:</i></color>" + ScoreBase.PlayerScore.mPlayerKills + "\n" +
           "<color=blue>Citizens saved:</color> " + ScoreBase.PlayerScore.mPlayerCivsSaved + "\n" +
           "<color=green>XP:</color> " + ScoreBase.PlayerScore.mXP + "\n" +
           "Grade: <color=green>A</color>"; // keep this constant.
    }
}
