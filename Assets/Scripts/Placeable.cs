﻿using UnityEngine;
using System.Collections;

public class Placeable : MonoBehaviour 
{

    bool placed = false;
    Vector3 mousePosition;

	void Start () 
    {
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Init();
	}

	void Update () 
    {
        
	    if(!placed)
        {
            if(Input.GetMouseButtonUp(0))
            {
                placed = true;
                ScoreBase.PlayerScore.AddPoints(ScoreBase.PlayerScore.PLACEMENTBONUS, gameObject.name + ", construction complete!");
                Activate();
            }

            //Plane plane = new Plane(Camera.main.transform.forward, 0);
            Plane plane = new Plane(Vector3.up, 0);
            float dist;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (plane.Raycast(ray, out dist))
            {
                Vector3 point = ray.GetPoint(dist);
               // point.z += 0.05f;
                transform.position = point;
            }

        }
        else
        {
            ActivatedUpdate();
            
        }
	}

    internal virtual void Init() { }
    internal virtual void ActivatedUpdate() { }
    internal virtual void Activate() { }
}
