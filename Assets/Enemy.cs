﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    Game ParentSpawner;

	const float SPEED = 10;
	const float ROTATION_SPEED = 90;
	const float RADIUS = 0.5f;
	Quaternion ROTATE_LEFT = Quaternion.Euler(0, -90, 0);
	Quaternion ROTATE_RIGHT = Quaternion.Euler(0, 90, 0);
	float sin, sin2;
	float randSpeed;

    public Vector2 MinBoundingBoxPos;
    public Vector2 MaxBoundingBoxPos;
    public GameObject DeathSplat;

    private DamageBase DamageBase;
    public LayerMask mask;
    float lifespan = 30f;
    
    void OnDeath()
    {
        ParentSpawner.EnemiesDied();
        Instantiate(DeathSplat, transform.position, DeathSplat.transform.rotation);
        Destroy(gameObject);
    }

    void Start () {
		sin = Random.Range (0, 360);
		sin2 = Random.Range (0, 360);
		randSpeed = Random.Range (0, 5);
        DamageBase = GetComponent<DamageBase>();
        DamageBase.OnDeath += OnDeath;

        ParentSpawner = GameObject.Find("The Game Master").GetComponent<Game>();
        StartCoroutine(Lifespan());
    }

    IEnumerator Lifespan()
    {
        yield return new WaitForSeconds(lifespan);
        OnDeath();
    }
    
	
	void Update () {
		sin += 90 * Time.deltaTime;
		sin2 += 90 * Time.deltaTime;

		float speed = SPEED + randSpeed;
		speed += Mathf.Sin (sin * Mathf.Deg2Rad) * 2;
		float totalDist = speed * Time.deltaTime;



        if (transform.position.x < MinBoundingBoxPos.x || transform.position.x > MaxBoundingBoxPos.x)
        {
            ScoreBase.PlayerScore.EnemyMadeItToTheEnd();
            ParentSpawner.EnemiesDied();
            Destroy(this.gameObject);
        }
        

        while (totalDist > 0) {
			RaycastHit hit;
			if (!Physics.SphereCast (transform.position, RADIUS, transform.forward, out hit, totalDist, mask)) {
                

				transform.position = transform.position + (transform.forward * totalDist);

                if (!Physics.SphereCast(transform.position, RADIUS - 0.1f, Vector3.right, out hit, 1, mask))
                {
					float deg = Mathf.Sin (sin2 * Mathf.Deg2Rad) * 0.5f;
					Vector3 right = new Vector3 (1, 0, deg).normalized;
					transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(right), ROTATION_SPEED * Time.deltaTime);
				}

				return;
			}

			transform.position = transform.position + (transform.forward * hit.distance);
			totalDist -= hit.distance;

            

            float angle = Vector3.Angle (transform.forward, hit.normal);
			angle *= Mathf.Sign(Vector3.Cross(transform.forward, hit.normal).y);

			if (angle < 0) {
				transform.rotation = Quaternion.LookRotation (ROTATE_RIGHT * hit.normal, Vector3.up);
			} else {
				transform.rotation = Quaternion.LookRotation (ROTATE_LEFT * hit.normal, Vector3.up);
			}
		}
    }
}