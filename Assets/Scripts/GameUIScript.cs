﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUIScript : MonoBehaviour {

    //public Text MoralTextObject;
    public GameObject ScrapButtion;

    float OldMoralNumber = 0.0f;
    float MoralNumberForGordon = 0.0f;

    public GameObject Turret;
    public GameObject Wedge;
    public GameObject Tank;
    public GameObject Civilian;

    GameObject SelectedBuilding;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        UpdateMoral();
    }

    void UpdateMoral()
    {
        float NewMoralNumber = 100.0f;  
        if (MoralNumberForGordon != 0.0f)
        {
            NewMoralNumber = MoralNumberForGordon;
            MoralNumberForGordon = 0.0f;
        }
        else
        {
            // Get Moral Number Here
        }

        if (OldMoralNumber != NewMoralNumber)      
        {
            //MoralTextObject.text = "Moral " + NewMoralNumber + "%";
            OldMoralNumber = NewMoralNumber;
        }
    }

    public void ButtionEvents(string ButtionName)
    {
        ScoreBase sb = ScoreBase.PlayerScore;
        if(ButtionName == "Wedge")
        {
            if (sb.ConsumeScrap(10))
            {
                GameObject TurretWedge = Instantiate(Wedge) as GameObject;
                TurretWedge.name = "Wedge";
            }
        }

        if (ButtionName == "Turret")
        {
            if (sb.ConsumeScrap(25))
            {
                GameObject TurretTemp = Instantiate(Turret) as GameObject;
                TurretTemp.name = "Turret";
            }
        }

        if (ButtionName == "Tank")
        {
            if (sb.ConsumeScrap(100))
            {
                GameObject TankTemp = Instantiate(Tank) as GameObject;
                TankTemp.name = "Tank";
            }
        }

        if(ButtionName == "Scrap" && SelectedBuilding != null)
        {
            ScoreBase.PlayerScore.AddScrap(10);
            Instantiate(Civilian).transform.position = SelectedBuilding.transform.position;
            DeSelectedScrapOption();
       }
        else
        {
            SelectedBuilding = null;
        }
    }

    public void SetMoralText(float MoralNumber)
    {
        MoralNumberForGordon = MoralNumber;
    }

    public void PopUpScrapOption(GameObject Building, Vector2 MousePos)
    {
        SelectedBuilding = Building;

        // here is where the scrap option appeares 
        Vector2 Offset = new Vector2(20.0f, 20.0f);
        float ScreenBoarderX = Screen.width * 0.25f;
        float ScreenBoarderY = Screen.height * 0.25f;
        if (MousePos.x > Screen.width - ScreenBoarderX)
        {
            Offset.x = -Offset.x;
        }
        if(MousePos.y > Screen.height - ScreenBoarderY)
        {
            Offset.y = -Offset.y;
        }
        ScrapButtion.SetActive(true);
        ScrapButtion.transform.position  = MousePos + Offset;
    }

    public void DeSelectedScrapOption()
    {
        Destroy(SelectedBuilding);
        SelectedBuilding = null;

        ScrapButtion.SetActive(false);
    }
}
