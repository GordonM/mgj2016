﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BuildingPhaseScript))]
public class GameManagerScript : MonoBehaviour {

    enum GameStates { LoadingIn, BuildingPhase, ZombieAttackTime, NextPhase, Repeat, End} // i guess the last one is more command than a state :P

    GameStates OldGameState;
    GameStates CurrentGameState;
    GameStates NextGameState;

    public GameObject UIEndLevel;

    Game EnemiesPhaseScript;
    BuildingPhaseScript BuildingScript;

    //
    // The broken ass light
    //

    public GameObject mLightObject;
    public UnityEngine.Light mDirectionalLight;

    //
    // Light rotational system
    //

    public Vector3 MorningLightRotation;
    public Color MorningLightColour;
    public Vector3 NightLightRotation;
    public Color NightLightColour;
    bool IsDay = false;

    public string GameStateName;
    

    // Use this for initialization
    void Start () {
        //DirLight = 
        EnemiesPhaseScript = FindObjectOfType<Game>();
        BuildingScript = GetComponent<BuildingPhaseScript>();


        OldGameState = GameStates.Repeat;
        CurrentGameState = GameStates.LoadingIn;
       // NextGameState = GameStates.Repeat;

        NextGameState = GameStates.BuildingPhase;

        IsDay = false;
        ChangeTimeofDay();
    }
	
	// Update is called once per frame
	void Update () {
        CheckState();
        GameStateName = "" + CurrentGameState;  
    }

    void CheckState()
    {
        if (CurrentGameState != OldGameState || CurrentGameState == GameStates.End)
        {
            switch (CurrentGameState)
            {
                case GameStates.LoadingIn:
                    LoadingEachLevel();
                    break;

                case GameStates.BuildingPhase:
                    BuildingScript.NewDay();
                    ChangeTimeofDay();
                    break;

                case GameStates.ZombieAttackTime:
                    EnemiesPhaseScript.SpawnEnemies();
                    ChangeTimeofDay();  
                    break;

                case GameStates.NextPhase:
                    break;

                case GameStates.Repeat:
                    // still dont know why this is a state
                    break;

            }
            OldGameState = CurrentGameState;
        }

        if (NextGameState != GameStates.Repeat)// i found a use for it \o/
        {
            if(CurrentGameState != GameStates.End)
            {
                CurrentGameState = NextGameState;
            }
            
            NextGameState = GameStates.Repeat;
        }
    }
    public void EndGame()
    {
        NextGameState = GameStates.End;
    }
    void ChangeTimeofDay()
    {
        if(IsDay)
        {
            mDirectionalLight.transform.rotation = Quaternion.Euler(NightLightRotation);
            mDirectionalLight.color = NightLightColour;
            IsDay = false;
        }
        else
        {
            mDirectionalLight.transform.rotation = Quaternion.Euler(MorningLightRotation);
            mDirectionalLight.color = MorningLightColour;
            IsDay = true;
        }
    }

    void LoadingEachLevel()
    {
        ScoreBase.PlayerScore.StatingLevelAmount();
        EnemiesPhaseScript.newLevelSetup(5); // 5 for the first Level // sort this out for more levels
        NextGameState = GameStates.BuildingPhase;
    }

    public void EndBuildingPhase()
    {
        NextGameState = GameStates.ZombieAttackTime;
    }

    public void EndZombieAttackPhase(bool EndOfWaves)
    {
        if(EndOfWaves)
        {
            // end of levelScreen
            StartCoroutine("EndOfWaves");
        }
        else
        {
            NextGameState = GameStates.BuildingPhase;
        }
    }

    public void RestartLevel()
    {
        OldGameState = GameStates.Repeat;
        CurrentGameState = GameStates.LoadingIn;

        NextGameState = GameStates.BuildingPhase;

        IsDay = false;
        ChangeTimeofDay();
    }

    IEnumerator EndOfWaves()
    {

        // GAME OVER NO MORE ZOMBIES.
        yield return new WaitForSeconds(5.0f);

        UIEndLevel.SetActive(true);
    }
}
