﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class GunTurret : Placeable 
{

    int maxMagSize = 4;
    int currAmmo;
    float reloadSpeed = 10f;
    float firingRate = 0.75f;
    bool firing = false;
    bool reloading = false;

    //Animation anim;
    Animator anim;
   //    bool animActivePlayed = false;
    bool ready = false;

    public GameObject Ammo;
    public GameObject[] RocketSpawns;
    int rocketpoint = 1;

    public AudioSource soundActivate;
    public AudioSource soundPassive;

    GameObject RocketHolder;

    internal override void Init()
    {
        currAmmo = maxMagSize;
        anim = GetComponent<Animator>();
        soundActivate = GetComponent<AudioSource>();
    }

    internal override void Activate()
    {
        anim.SetTrigger("activate");
        soundActivate.Play();
    }

    internal override void ActivatedUpdate()
    {
        if (!ready) return;

        if (currAmmo > 0 && !firing)
        {
            StartCoroutine(Fire());
        }
        if (currAmmo == 0 && !reloading)
        {
            StartCoroutine(Reload());
        }
    }

    public void ActivateAnimationFinished()
    {
        ready = true;
        soundActivate.Stop();
        soundPassive.Play();
    }
        
    IEnumerator Fire()
    {

        if(RocketHolder == null)
        {
            RocketHolder = new GameObject("Rocket Holder");
            RocketHolder.transform.parent = this.transform;
        }

        firing = true;
        yield return new WaitForSeconds(firingRate);
        // Debug.Log("BULLET FIRED: "+ currAmmo + " ammo left. " + Time.time);
        currAmmo--;

        rocketpoint++;
        if (rocketpoint > 4) rocketpoint = 1;

       // soundFire.Play();

        GameObject RocketTemp = Instantiate(Ammo, RocketSpawns[rocketpoint-1].transform.position, Ammo.transform.rotation) as GameObject;
        RocketTemp.transform.parent = RocketHolder.transform;
        firing = false;
    }

    IEnumerator Reload()
    {
        reloading = true;
        // Debug.Log("RELOADING");
        yield return new WaitForSeconds(reloadSpeed);
        currAmmo = maxMagSize;
        // Debug.Log("RELOADED");
        reloading = false;
    }
}
