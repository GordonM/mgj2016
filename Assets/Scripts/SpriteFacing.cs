﻿using UnityEngine;
using System.Collections;

public class SpriteFacing : MonoBehaviour {

    Quaternion CameraRotation;

    public Sprite[] ZombieSprites;

    float Sway;

    float MaxSwayingRotation;
    float CurrentSwayingAmount;
    float SwayingAmount;
    public Vector3 RotationAroundPoint;

	// Use this for initialization
	void Start () {
        CameraRotation = GameObject.Find("Main Camera").transform.rotation;
        int rand = Random.Range(0, ZombieSprites.Length);

        SpriteRenderer Render = GetComponent<SpriteRenderer>();
        Render.sprite = ZombieSprites[rand];

        Sway = Mathf.Sign(Random.Range(-1.0f, 2.0f));
        MaxSwayingRotation = 10.0f;
        SwayingAmount = 1.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.rotation = CameraRotation;

        SwaySprite();
    }

    void SwaySprite()
    {
        Vector3 ZAxis = new Vector3(0.0f, 0.0f, 1.0f);
        if (Sway > 0.0f)
        {
            CurrentSwayingAmount += SwayingAmount;
            if (CurrentSwayingAmount >= MaxSwayingRotation)
            {
                Sway *= -1;
            }
            transform.Rotate(ZAxis, CurrentSwayingAmount);
        }
        else
        {
            CurrentSwayingAmount -= SwayingAmount;
            if (CurrentSwayingAmount <= -MaxSwayingRotation)
            {
                Sway *= -1;
            }
            transform.Rotate(ZAxis, CurrentSwayingAmount);
        }
    }

   void OnDrawGizmos()
    {
        Vector3 sizeX = new Vector3(0.5f, 0.0f, 0.0f);
        Vector3 sizeY = new Vector3(0.0f, 0.5f, 0.0f);
        Vector3 sizeZ = new Vector3(0.0f, 0.0f, 0.5f);

        Gizmos.DrawLine(RotationAroundPoint + sizeX, RotationAroundPoint - sizeX);
        Gizmos.DrawLine(RotationAroundPoint + sizeY, RotationAroundPoint - sizeY);
        Gizmos.DrawLine(RotationAroundPoint + sizeZ, RotationAroundPoint - sizeZ);
    }
}
