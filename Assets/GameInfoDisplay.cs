﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// This is a cheap way of limiting a console style prompt for the player, this displays information / interaction and their xp bonuses.
/// </summary>
public class GameInfoDisplay : MonoBehaviour {
    /// <summary>
    /// A reference to the only existant object.
    /// </summary>
    public static GameInfoDisplay display;
    Text mInfoDisplay;

	// Use this for initialization
	void Start () {
        mInfoDisplay = GetComponent<Text>();
        DisplayData = new Queue<string>();
        AddMessage("Please prepare for impending attack, protect your citizens and fight back; we're going to need you in future!");
        // reference for use anywhere
        display = this; // script.
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Stack container for the data display.
    /// </summary>
    Queue<string> DisplayData;    

    /// <summary>
    /// Lines of information to show to the player, max, extra will be dropped.
    /// </summary>
    public int MaximumDisplayLines = 5;

    /// <summary>
    /// Add a message to the display.
    /// </summary>
    /// <param name="str"></param>
    public void AddMessage(string str)
    {
        // Queue the information in the table.
        DisplayData.Enqueue(str);

        if(DisplayData.Count > MaximumDisplayLines && DisplayData.Count > 1)
        {
            // Remove one of our strings.
            DisplayData.Dequeue();
        }

        // Erase console / doesn't matter
        mInfoDisplay.text = "";

        // Regenerate the message information based on our queue.
        foreach( string K in DisplayData )
        {
            // Add line to the info.
            mInfoDisplay.text += K + "\n";
        }        
    }


   



}
