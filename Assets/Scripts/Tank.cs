﻿using UnityEngine;
using System.Collections;

public class Tank : Placeable {
    
    bool firing = false;
    float fireRate = 1f;
    float engagementRadius = 50f;
    public LayerMask mask;
    public GameObject TankShell;

	// Use this for initialization
	void Start () 
    {

	}
	
    internal override void ActivatedUpdate()
    {
        if (firing) return;
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, engagementRadius, mask.value);
        float minDist = engagementRadius;
        Vector3 target = Vector3.zero;
        foreach(Collider hit in hitColliders)
        {
            if(Vector3.Distance(hit.transform.position, gameObject.transform.position) < minDist)
            {
                target = hit.transform.position;
                minDist = Vector3.Distance(hit.transform.position, gameObject.transform.position);
            }
        }
        if (target == Vector3.zero) return; //no target found
        StartCoroutine(Fire(target));
        StartCoroutine(Turn(target));
    }

    IEnumerator Turn(Vector3 target)
    {
        transform.LookAt(target);
        transform.forward = -transform.forward;
        yield return null;
    }

    IEnumerator Fire(Vector3 target)
    {
        firing = true;
        yield return new WaitForSeconds(fireRate);
        GameObject tankShell = Instantiate(TankShell);
        tankShell.transform.position = transform.position;
        tankShell.GetComponent<TankShell>().SetTarget(target);
        firing = false;
    }
}
