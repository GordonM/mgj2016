﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RewardManager : MonoBehaviour
{
    public GameObject LevelOneHolder;
    public GameObject LevelTwoHolder;

    public GameObject GameManager;
    GameManagerScript ManagerScript;

    // Use this for initialization
    void Start()
    {
        ManagerScript = GameManager.GetComponent<GameManagerScript>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ButtionEvents(string ButtionName)
    {
        if (ButtionName == "Menu")
        {
            SceneManager.LoadScene("MenuScene");
        }

        if (ButtionName == "Level One")
        {
            LevelTwoHolder.SetActive(false);
            LevelOneHolder.SetActive(true);
            ManagerScript.RestartLevel();
        }

        if (ButtionName == "Level Two")
        {
            LevelOneHolder.SetActive(false);
            LevelTwoHolder.SetActive(true);
            ManagerScript.RestartLevel();
        }
    }
}
