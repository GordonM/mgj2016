﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class XPDisplay : MonoBehaviour {
    Text XPText;
	// Use this for initialization
	void Start () {
        XPText = GetComponent<Text>();
        // Propigate update.
        ScoreBase.PlayerScore.OnScoreUpdate += OnScore;
        // Make sure we've got the latest value, don't rely upon this during initialisation of the game.
        ScoreBase.PlayerScore.UpdateScoreInfo();
	}
	
	// Update is called once per frame
	void OnScore ( float xp, float sc ) {
        // Thousands column handling.
	    XPText.text = string.Format("{0:n0}", xp) + " XP" + "\n " +
                      string.Format("{0:n0}", sc) + " SC";
    }
}
