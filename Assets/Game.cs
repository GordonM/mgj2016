﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	public Transform pfEnemy;
    GameObject EnemyHolder;
        
    Coroutine waveSys;
    GameManagerScript GameManager;

    public AudioClip[] soundZombies;
    AudioSource audioSource;
    bool playingZombieSound = false;
    
    float WavesLeft;

    int AmountOfEnemies;

    void Start () 
    {
        GameManager = GetComponent<GameManagerScript>();
        audioSource = GetComponent<AudioSource>();
        if (EnemyHolder == null)
        {
            EnemyHolder = new GameObject("Enemy Holder");
        }
        
	}

    void Update()
    {
        if(EnemyHolder.transform.childCount > 0 && !playingZombieSound)
        {
            StartCoroutine(PlayZombieSound());
        }
        
    }

    /// <summary>
    /// REPLACED WITH CO-ROUTINE TO INCREMENTALLY FUCK THE PLAYER WITH WAVES.
    /// </summary>
    public void SpawnEnemies()
    {
        /// BEGIN THE DOOOOM SPAWN THE WAVES
        waveSys = StartCoroutine(SpawnWave());
    }

    public void EnemiesDied()
    {
        bool WaveEnd = false;
        AmountOfEnemies -= 1;
        if (AmountOfEnemies <= 0)
        {
            if (WavesLeft <= 0)
            {
                WaveEnd = true;
            }
            GameManager.EndZombieAttackPhase(WaveEnd);
        }
    }

    public void newLevelSetup(int WavePerLevel)
    {
        WavesLeft = WavePerLevel;
    }

    IEnumerator SpawnWave()
    {
        AmountOfEnemies = 0;
        //do
        //{
        --WavesLeft;
        
        // wave start
        for (float x = -120; x <= -100; x += 10)
        {
            for (float z = -38.5f; z <= 38.5f; z += 10)
            {
                Vector3 pos = new Vector3(x, 0, z);
                Transform enemy = Instantiate(pfEnemy, pos, Quaternion.Euler(0, 90, 0)) as Transform;
                enemy.parent = EnemyHolder.transform;
                AmountOfEnemies += 1;

            }
        }

        GameInfoDisplay.display.AddMessage("RADIO: Incoming WAVE; <color=blue>think fast!</color>");
        // Start timing for next wave.
        // yield return new WaitForSeconds(TimeBetweenWaves);
        //} while (WavesLeft > 0);
        yield return null;

       // yield return new WaitForSeconds(0.0);
    }

    IEnumerator PlayZombieSound()
    {
        playingZombieSound = true;

        audioSource.clip = soundZombies[Random.Range(0, soundZombies.Length)];
        audioSource.Play();
        yield return new WaitForSeconds(audioSource.clip.length);

        playingZombieSound = false;
    }
}

