﻿using UnityEngine;
using System.Collections;

public class TankShell : MonoBehaviour {

    float moveSpeed = 5f;
    float minDamageRadius = 5f;
    float maxDamageRadius = 15f;
    float damage = 45.0f;
    float splashRatio = 0.5f;
    Vector3 target = Vector3.zero;
    public LayerMask mask;
    public AudioSource soundExplode;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (target == Vector3.zero) return;
        transform.position = Vector3.Lerp(transform.position, target, moveSpeed * Time.deltaTime);
        if(Vector3.Distance(transform.position, target) < moveSpeed)
        {
            Detonate();
        }
	}

    void DamageArea(float radius, float dam)
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius, mask.value);

        foreach (Collider hit in hitColliders)
        {
            DamageBase script = hit.GetComponent<DamageBase>();
            if (!script) break;
            script.Damage(dam, gameObject);
        }
    }

    private void Detonate()
    {
        //hit anything in min radius
        DamageArea(minDamageRadius, damage);
        //hit anything inn max radius
        DamageArea(maxDamageRadius, damage * splashRatio);
        //Spawn audio and effects
        PlayClipAtPoint(soundExplode.clip, transform.position);
        Destroy(gameObject);
    }

    internal void SetTarget(Vector3 tar)
    {
        target = tar;
    }

    void PlayClipAtPoint(AudioClip clip, Vector3 pos)
    {
        GameObject temp = new GameObject("TempAudio");
        temp.transform.position = pos;
        AudioSource audioSource = temp.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.outputAudioMixerGroup = soundExplode.outputAudioMixerGroup;
        audioSource.Play();
        Destroy(temp, clip.length);
    }
}
