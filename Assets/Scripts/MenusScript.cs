﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenusScript : MonoBehaviour {

    enum MenuStates { Main, Options, LevelSelect};
    MenuStates menuState;

    public GameObject MainMenu;
    public GameObject OptionsMenu;
    public GameObject LevelSelectMenu;
    
    // Use this for initialization
    void Start () {
        menuState = MenuStates.Main;
        ChangeScene("Main Menu");
    }
	
	// Update is called once per frame
	void Update () {

	}
    

    public void ButtionEvents(string ButtionName)
    {
        if (menuState == MenuStates.Main)
        {
            if (ButtionName == "Play")
            {
                ChangeScene("Level Select");
            }
            else if (ButtionName == "Options")
            {
                ChangeScene(ButtionName);
            }
            else if (ButtionName == "Quit")
            {
                Application.Quit(); 
            }
        }
        else if (menuState == MenuStates.Options)
        {
            if (ButtionName == "Back")
            {
                ChangeScene("Main Menu");
            }
        }
        else if (menuState == MenuStates.LevelSelect)
        {
            if (ButtionName == "Level One")
            {
                SceneManager.LoadScene("Staging");
            }

            if (ButtionName == "Back")
            {
                ChangeScene("Main Menu");
            }
        }
    }

    void ChangeScene(string SceneName)
    {
        MainMenu.SetActive(false);
        OptionsMenu.SetActive(false);
        LevelSelectMenu.SetActive(false);

        if (SceneName == "Main Menu")
        {
            MainMenu.SetActive(true);
            menuState = MenuStates.Main;
        }
        else if(SceneName == "Level Select")
        {
            LevelSelectMenu.SetActive(true);
            menuState = MenuStates.LevelSelect;
        }
        else if (SceneName == "Options")
        {
            OptionsMenu.SetActive(true);
            menuState = MenuStates.Options;
        }
    }
}
